CREATE TABLE app_messages
(
    id bigint PRIMARY KEY ,
    sender_id bigint,
    recipient_id bigint,
    text varchar(500),
    FOREIGN KEY (recipient_id) REFERENCES users(id),
    FOREIGN KEY (sender_id) REFERENCES users(id)
);