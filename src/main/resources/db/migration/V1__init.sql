CREATE TABLE users
(
                       id bigint PRIMARY KEY ,
                       username varchar(100) NOT NULL,
                       first_name varchar(50) NOT NULL,
                       password varchar(50) NOT NULL,
                       last_login timestamp,
                        user_rank varchar(50),
                       constraint username_unique UNIQUE (username)
);