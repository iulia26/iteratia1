CREATE TABLE tasks
(
    id bigint PRIMARY KEY ,
    description varchar(300),
    status varchar(50),
    recipient_id bigint,
     FOREIGN KEY (recipient_id) REFERENCES users(id)
);