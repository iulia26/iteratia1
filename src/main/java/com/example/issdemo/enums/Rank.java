package com.example.issdemo.enums;

public enum Rank {
    OWNER("OWNER"),
    EMPLOYEE("EMPLOYEE"),
    UNKNOWN("UNKNOWN");

    private String code;

    private Rank(String code) {
        this.code = code;
    }

    public String getRank() {
        return code;
    }

}
