package com.example.issdemo.enums;

public enum Status {
    FINISHED("FINISHED"),
    IN_PROGRESS("IN_PROGRESS");

    private String code;

    private Status(String code) {
        this.code = code;
    }

    public String getStatus() {
        return code;
    }
}
