package com.example.issdemo.domain;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "app_messages")
public class MyMessage {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;


    @OneToOne
    @JoinColumn(name = "sender_id",referencedColumnName = "id")
    private User sender;


    @OneToOne
    @JoinColumn(name = "recipient_id",referencedColumnName = "id")
    private User recipient;


    @Size( max = 500)
    private String text;
}
