package com.example.issdemo.domain;

import com.example.issdemo.enums.Status;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Entity
@Data
@Table(name = "tasks")
@NoArgsConstructor
@AllArgsConstructor
public class Task {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;


    @Size(max = 300)
    private String description;


    @Enumerated(EnumType.STRING)

    private Status status;

    @OneToOne
    @JoinColumn(name = "recipient_id",referencedColumnName = "id")
    private User recipient;


}
