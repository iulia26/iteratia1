package com.example.issdemo.domain;


import com.example.issdemo.enums.Rank;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;

@Entity
@Table(name = "users")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank
    @Column(unique = true)
    @Size(min = 1, max = 100)
    private String username;


    @Size(max = 50)
    private String firstName;


    @Size(min=1,max = 50)
    private String password;

    private LocalDateTime lastLogin;


    @Enumerated(EnumType.STRING)
    private Rank rank;

}
