package com.example.issdemo;

import com.example.issdemo.domain.Task;
import com.example.issdemo.domain.User;
import com.example.issdemo.enums.Rank;
import com.example.issdemo.repos.IRepoTasks;
import com.example.issdemo.repos.IRepoUsers;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

import java.util.Arrays;

@SpringBootApplication
public class IssDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(IssDemoApplication.class, args);
    }

    private static  void sayHello(IRepoUsers repoUsers,IRepoTasks repoTasks){
        System.out.println("==========================ANABEL====================================");
        User testusr = repoUsers.findById(1L).orElse(null);
        if(testusr != null){
            if(testusr.getRank().equals(Rank.EMPLOYEE)){
                System.out.println("YES SURE JE IS EMPLYEE");
            }
            System.out.println(testusr);
        }

        Task testtask = repoTasks.findById(1L).orElse(null);
        if(testtask != null){
            System.out.println(testtask);
        }
    }

    @Bean
    CommandLineRunner run(IRepoUsers repoUsers, IRepoTasks repoTasks){

        return args -> {
            sayHello(repoUsers,repoTasks);

        };

    }

    @Bean
    public CorsFilter corsFilter(){
        UrlBasedCorsConfigurationSource urlBasedCorsConfigurationSource  = new UrlBasedCorsConfigurationSource();
        CorsConfiguration corsConfiguration = new CorsConfiguration();
        corsConfiguration.setAllowCredentials(true);
        corsConfiguration.setAllowedOrigins(Arrays.asList("http://localhost:3000","http://localhost:4200"));
        corsConfiguration.setAllowedHeaders(Arrays.asList("Origin","Access-Control-Allow-Origin","Content-Type","Accept","Jwt-Token","Authorization","Origin, Accept","X-Requested-With","Access-Control-Request-Method"
                ,"Access-Control-Request-Headers"));
        corsConfiguration.setExposedHeaders(Arrays.asList("Origin","Content-Type","Accept","Jwt-Token","Authorization","Access-Control-Allow-Origin","Access-Control-Allow-Origin","Access-Control-Allow-Credentials","Filename"));
        corsConfiguration.setAllowedMethods(Arrays.asList("GET","PUT","POST","PATCH","DELETE","OPTIONS"));
        urlBasedCorsConfigurationSource.registerCorsConfiguration("/**",corsConfiguration);
        return new CorsFilter(urlBasedCorsConfigurationSource);

    }
}