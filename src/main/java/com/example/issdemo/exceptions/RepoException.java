package com.example.issdemo.exceptions;

public class RepoException extends Exception{

        public RepoException() {
            super();
        }

        public RepoException(String message) {
            super(message);
        }

        public RepoException(String message, Throwable cause) {
            super(message, cause);
        }
    }


