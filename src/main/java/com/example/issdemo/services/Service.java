package com.example.issdemo.services;

import com.example.issdemo.domain.*;
import com.example.issdemo.enums.Rank;
import com.example.issdemo.enums.Status;
import com.example.issdemo.exceptions.RepoException;
import com.example.issdemo.exceptions.ServiceException;
import com.example.issdemo.repos.IRepoMessages;
import com.example.issdemo.repos.IRepoNotifications;
import com.example.issdemo.repos.IRepoTasks;
import com.example.issdemo.repos.IRepoUsers;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;


@RequiredArgsConstructor
@org.springframework.stereotype.Service
@Slf4j
public class Service {

    private final IRepoUsers repoUsers;
    private final IRepoTasks repoTasks;
    private final IRepoMessages repoMessages;
    private final IRepoNotifications repoNotifications;

    public Optional<User> logIn(User user) throws RepoException, ServiceException {
        log.info("Starting login...");
        if(user == null)throw  new ServiceException("User can't be null");

        log.info("Start checking username");
        Optional<User> dbUser = repoUsers.findByUsername(user.getUsername());
        if(dbUser.isEmpty()) throw  new RepoException("Invalid username");
        log.info("Start checking password");
        if(user.getPassword().equals(dbUser.get().getPassword())) {
            dbUser.get().setLastLogin(LocalDateTime.now());
            repoUsers.save(dbUser.get());
            log.info("Finished login");
            return repoUsers.findById(dbUser.get().getId());
        }

        throw  new RepoException("Invalid password");

    }

    public Optional<User> findById(Long id){
        return repoUsers.findById(id);
    }

    public List<User> getAllEmployees(){
        return repoUsers.findAll().stream().filter(user -> user.getRank().equals(Rank.EMPLOYEE)).collect(Collectors.toList());
    }

    public void sendTask(Task task) throws RepoException {
        task.setStatus(Status.IN_PROGRESS);
        Optional<User> result = repoUsers.findByUsername(task.getRecipient().getUsername());
        if(result.isEmpty()){
            throw  new RepoException("User does not exist in database");
        }
        task.setRecipient(result.get());

        repoTasks.save(task);

    }

    public List<Task> getTaskForSpecificUser(User user){
       return  repoTasks.findAll().stream().filter(elem -> elem.getRecipient().getUsername().equals(user.getUsername())).collect(Collectors.toList());
    }

    public void finishTask(Task task){
        Optional<Task> original = repoTasks.findById(task.getId());
        Task my = original.get();
        my.setStatus(Status.FINISHED);
        repoTasks.save(my);
    }

    public List<String> getNewNotifications(){
        List<Notification> results =  repoNotifications.findAll();

        return results.stream().map(Notification::getDescription).collect(Collectors.toList());

    }

    public void addNewNotification(String username){
            repoNotifications.save(new Notification(0L,username));
    }


    public List<MessageDTO> getMessages(String firstUser, String secondUser) {

        return repoMessages.findAll().stream().filter(message ->
          (message.getRecipient().getUsername().equals(firstUser) && message.getSender().getUsername().equals(secondUser))
                    || (message.getRecipient().getUsername().equals(secondUser) && message.getSender().getUsername().equals(firstUser))
        ).map(myMessage -> new MessageDTO(myMessage.getSender().getUsername(), myMessage.getText(),myMessage.getRecipient().getUsername())).collect(Collectors.toList());

    }

    public void saveMessage(MessageDTO message) {
        repoMessages.save(new MyMessage(0L,repoUsers.findByUsername(message.getFrom()).get(),repoUsers.findByUsername(message.getTo()).get(),message.getText()));
    }
}
