package com.example.issdemo.repos;

import com.example.issdemo.domain.MyMessage;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IRepoMessages extends JpaRepository<MyMessage,Long> {
}
