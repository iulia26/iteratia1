package com.example.issdemo.repos;

import com.example.issdemo.domain.Notification;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IRepoNotifications extends JpaRepository<Notification,Long> {
}
