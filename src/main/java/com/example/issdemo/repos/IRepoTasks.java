package com.example.issdemo.repos;

import com.example.issdemo.domain.Task;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IRepoTasks extends JpaRepository<Task,Long> {

}
