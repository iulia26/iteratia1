package com.example.issdemo.controllers;

import com.example.issdemo.domain.MessageDTO;
import com.example.issdemo.domain.MyMessage;
import com.example.issdemo.domain.Task;
import com.example.issdemo.domain.User;
import com.example.issdemo.enums.Rank;
import com.example.issdemo.enums.Status;
import com.example.issdemo.exceptions.RepoException;
import com.example.issdemo.exceptions.ServiceException;
import com.example.issdemo.model.Response;
import com.example.issdemo.services.Service;
import lombok.RequiredArgsConstructor;


import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.rmi.ServerException;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping("/api")
@RequiredArgsConstructor
@Slf4j
public class ServerResource {

    private final Service appService;

    @PostMapping("/login")
    public ResponseEntity<Response> logIn(@RequestBody User user ){
        log.info("In logIn from resource serve class");
        System.err.println("In log in");
        try{
            Optional<User> loggedInUser = appService.logIn(user);
            log.info("Exiting request for login");
            return ResponseEntity.status(HttpStatus.OK).body(
                    Response.builder().data(Map.of("LoggedInUser",loggedInUser.orElse(new User())))
                            .developerMessage("Login sucess")
                            .statusCode(HttpStatus.OK.value())
                            .build()

            );
        }catch (RepoException | ServiceException ex) {
            log.error(ex.getMessage());
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                    Response.builder().developerMessage(ex.getMessage()).statusCode(HttpStatus.BAD_REQUEST.value()).build()
            );
        }

    }

    @GetMapping("/employees")
    public ResponseEntity<Response> getEmployeesForAdmin(){
        log.info("In get employees");
       return ResponseEntity.status(HttpStatus.OK).body(
                Response.builder().data(Map.of("Employees",appService.getAllEmployees()))
                        .developerMessage("Retrieved employees")
                        .statusCode(HttpStatus.OK.value())
                        .build());
    }

    @PostMapping("/send-task")
        public ResponseEntity<Response> sendTask(@RequestBody Task task){
        log.info("In send task");
        try {
            appService.sendTask(task);
            return ResponseEntity.status(HttpStatus.OK).body(
                    Response.builder().developerMessage("Sended task")
                            .statusCode(HttpStatus.OK.value())
                            .build());
        }catch(RepoException ex){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                    Response.builder().developerMessage(ex.getMessage()).statusCode(HttpStatus.BAD_REQUEST.value()).build()
            );
        }
    }

    @GetMapping("/tasks/{username}")
    public ResponseEntity<Response>  getTasks(@PathVariable("username") Optional<String> username){
        log.info("Get tasks");

            return ResponseEntity.status(HttpStatus.OK).body(
                    Response.builder().data(Map.of("userTasks", appService.getTaskForSpecificUser(new User(0L, username.get(), null, null, null, null))))
                            .developerMessage("Login sucess")
                            .statusCode(HttpStatus.OK.value())
                            .build()

            );


    }


    //ATENTIE PENTRU URMATOARELE DATI,NU MAI FOLOSI CAMEL-CASE deci nu finishTask ,ci finish-task,asa se recomanda pentru REST API
    // si se necesita crearea unui controller pentru gestiunea fiecarei entitati din domeniul problemei
    //asa cum ai facut acum nu e corect,e code smell
    @PutMapping("/finish-task/{id}")
    public ResponseEntity<Response> finishTask(@PathVariable("id") Long id){
        log.info("finishing task...");
        appService.finishTask(new Task(id,null,null,null));
        return ResponseEntity.status(HttpStatus.OK).body(
                Response.builder()
                        .developerMessage("setted finished state")
                        .statusCode(HttpStatus.OK.value())
                        .build()

        );

    }

    @GetMapping("/new-notifications")
    public ResponseEntity<Response> getNewNotifications(){
        log.info("Getting new notifications...");

        return ResponseEntity.status(HttpStatus.OK).body(
                    Response.builder()
                            .data(Map.of("Notifications",appService.getNewNotifications()))
                            .developerMessage("Retrieved new notifs")
                            .statusCode(HttpStatus.OK.value())
                            .build()
        );
    }

    @GetMapping("/log-out/{username}")
    public ResponseEntity<Response> logOut(@PathVariable("username") String username){
        log.info("Log out");
        appService.addNewNotification(username);
        return ResponseEntity.status(HttpStatus.OK).body(
                Response.builder()
                        .developerMessage("logged out user")
                        .statusCode(HttpStatus.OK.value())
                        .build()

        );
    }

    @GetMapping("/messages")
    public ResponseEntity<Response> getMessages(@RequestParam("first") String firstUser,@RequestParam("second") String secondUser ){
        log.info("In get messages");
        List<MessageDTO> result = appService.getMessages(firstUser,secondUser);
        return ResponseEntity.status(HttpStatus.OK).body(
                Response.builder()
                        .data(Map.of("Messages",result))
                        .developerMessage("Retrieved messages")
                        .statusCode(HttpStatus.OK.value())
                        .build()

        );

    }

    @PostMapping ("/messages")
    public ResponseEntity<Response> sendMessage(@RequestBody MessageDTO message){
        log.info("In send Message");
        appService.saveMessage(message);

        return ResponseEntity.status(HttpStatus.OK).body(
                Response.builder()
                        .developerMessage("added message")
                        .statusCode(HttpStatus.OK.value())
                        .build()

        );

    }



}
